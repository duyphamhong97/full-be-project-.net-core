﻿namespace Scheduler
{
    using System;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.Azure.WebJobs;
    using Microsoft.Azure.WebJobs.Host;

    public static class DeadlineHandler
    {

        [FunctionName("DeadlineHandler")]
        public static async Task Run([TimerTrigger("0 */1 * * * *")]TimerInfo myTimer, TraceWriter log)
        {
            try
            {
                HttpClient client;
                client = new HttpClient();
                var uri = new Uri("http://localhost:60192/api/committee/deadline");
                //var uri = new Uri("http://cpaf-api.azurewebsites.net/api/committee/deadline");
                client.BaseAddress = uri;
                var buffer = Encoding.UTF8.GetBytes("");
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                await client.PutAsync("", byteContent);

                //api/advisor/deadline

                client = new HttpClient();
                uri = new Uri("http://localhost:60192/api/advisor/deadline");
                //uri = new Uri("http://cpaf-api.azurewebsites.net/api/advisor/deadline");
                client.BaseAddress = uri;
                await client.GetAsync(uri);

                //api/student/deadline

                client = new HttpClient();
                uri = new Uri("http://localhost:60192/api/student/deadline");
                //uri = new Uri("http://cpaf-api.azurewebsites.net/api/student/deadline");
                client.BaseAddress = uri;
                byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                await client.PutAsync("", byteContent);
            }
            catch (Exception e)
            {
                log.Info($"Error: {e}");
                throw e;
            }
        }
    }
}
